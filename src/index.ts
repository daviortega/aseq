import crypto from 'crypto';

const normalizeSequence = (sequence: string) => {
  return sequence.replace(/ /g, '').toUpperCase();
};

export default (sequence: string) => {
  const normSeq = normalizeSequence(sequence);
  const md5base64 = crypto
    .createHash('md5')
    .update(normSeq)
    .digest('base64');
  return md5base64
    .replace(/=+/g, '')
    .replace(/\+/g, '-')
    .replace(/\//g, '_');
};
