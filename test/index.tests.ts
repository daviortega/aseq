import { expect } from 'chai'

import aseq from '../src/index'

describe('aseq', function() {
  let fixtures = [
    {
      seqId: 'yg8A8H8N-4x1Ezf8WW-YbA',
      sequence: 'MTNVLIVEDEQAIRRFLRTALEGDGMRVFEAETLQRGLLEAATRKPDLIILDLGLPDGDGIEFIRDLRQWSAVPVIVLSARSEESDKIAALDAGADDYLSKPFGIGELQARLRVALRRHSATTAPDPLVKFSDVTVDLAARVIHRGEEEVHLTPIEFRLLAVLLNNAGKVLTQRQLLNQVWGPNAVEHSHYLRIYMGHLRQKLEQDPARPRHFITETGIGYRFML'
    },
    {
      seqId: 'naytI0dLM_rK2kaC1m3ZSQ',
      sequence: 'MNNEPLRPDPDRLLEQTAAPHRGKLKVFFGACAGVGKTWAMLAEAQRLRAQGLDIVVGVVETHGRKDTAAMLEGLAVLPLKRQAYRGRHISEFDLDAALARRPALILMDELAHSNAPGSRHPKRWQDIEELLEAGIDVFTTVNVQHLESLNDVVSGVTGIQVRETVPDPFFDAADDVVLVDLPPDDLRQRLKEGKVYIAGQAERAIEHFFRKGNLIALRELALRRTADRVDEQMRAWRGHPGEEKVWHTRDAILLCIGHNTGSEKLVRAAARLASRLGSVWHAVYVETPALHRLPEKKRRAILSALRLAQELGAETATLSDPAEEKAVVRYAREHNLGKIILGRPASRRWWRRETFADRLARIAPDLDQVLVALDEPPARTINNAPDNRSFKDKWRVQIQGCVVAAALCAVITLIAMQWLMAFDAANLVMLYLLGVVVVALFYGRWPSVVATVINVVSFDLFFIAPRGTLAVSDVQYLLTFAVMLTVGLVIGNLTAGVRYQARVARYREQRTRHLYEMSKALAVGRSPQDIAATSEQFIASTFHARSQVLLPDDNGKLQPLTHPQGMTPWDDAIAQWSFDKGLPAGAGTDTLPGVPYQILPLKSGEKTYGLVVVEPGNLRQLMIPEQQRLLETFTLLVANALERLTLTASEEQARMASEREQIRNALLAALSHDLRTPLTVLFGQAEILTLDLASEGSPHARQASEIRQHVLNTTRLVNNLLDMARIQSGGFNLKKEWLTLEEVVGSALQMLEPGLSSPINLSLPEPLTLIHVDGPLFERVLINLLENAVKYAGAQAEIGIDAHVEGENLQLDVWDNGPGLPPGQEQTIFDKFARGNKESAVPGVGLGLAICRAIVDVHGGTITAFNRPEGGACFRVTLPQQTAPELEEFHEDM'
    },
    {
      seqId: 'GS8z3QwN5MzpxU0aTuxuaA',
      sequence: 'MSGLRPALSTFIFLLLITGGVYPLLTTVLGQWWFPWQANGSLIREGDTVRGSALIGQNFTGNGYFHGRPSATAEMPYNPQASGGSNLAVSNPELDKLIAARVAALRAANPDASASVPVELVTASASGLDNNITPQAAAWQIPRVAKARNLSVEQLTQLIAKYSQQPLVKYIGQPVVNIVELNLALDKLDE'
    },
    {
      seqId: '1B2M2Y8AsgTpgAmY7PhCfg',
      sequence: ''
    },
    {
      seqId: 'NtBKnXQ5LHJ7Gpv5envLrA',
      sequence: 'AAAAAA'
    }
  ]

  const randomPosition = (string: string) => {
    return Math.floor(Math.random() * string.length)
  }

  const randomlyLowerCase = (sequence: string) => {
    let pos = randomPosition(sequence)
    return sequence.substr(0, pos) + sequence[pos].toLowerCase() + sequence.substr(pos + 1)
  }

  const randomlyInsertSpace = (sequence: string) => {
    let pos = randomPosition(sequence)
    return `${sequence.substr(0, pos)} ${sequence.substr(pos)}`
  }

  const changeCaseAndInjectWhitespace = (sequence: string) => {
    let result = sequence
    for (let i = 0; i < 10; i++) {
      result = randomlyLowerCase(result)
      result = randomlyInsertSpace(result)
    }

    return result
  }

  fixtures.forEach(function(fixture) {
    it(`${fixture.seqId} from ${fixture.sequence.substr(0, 32)}...`, function() {
      const id = aseq(fixture.sequence)
      expect(id).equal(fixture.seqId)
    })

    if (!fixture.sequence)
      return

    let mungedSequence = changeCaseAndInjectWhitespace(fixture.sequence)
    it(`${fixture.seqId} from ${mungedSequence.substr(0, 32)}...`, function() {
      const id = aseq(mungedSequence)
      expect(id).equal(fixture.seqId)
    })
  })
})