# aseq

![npm](https://img.shields.io/npm/v/@biowonks/aseq.svg)
![License: CC0-1.0](https://img.shields.io/badge/License-CC0%201.0-blue.svg)
[![pipeline status](https://gitlab.com/daviortega/aseq/badges/master/pipeline.svg)](https://gitlab.com/daviortega/aseq/commits/master)
[![coverage report](https://gitlab.com/daviortega/aseq/badges/master/coverage.svg)](https://daviortega.gitlab.io/aseq/)

A simple package to calculate aseq identifiers compatible with Biowonk projects (MiST3, SeqDepot and etc) with types.

## Install

`npm install @biowonks/aseq`

## Usage

```javascript
import makeAseq from '@biowonks/aseq'

const mySequence = 'AAAAAA'
const aseq = makeAseq(mySequence)

console.log(aseq)
// NtBKnXQ5LHJ7Gpv5envLrA
```

